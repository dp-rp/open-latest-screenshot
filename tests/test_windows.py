import pytest
from sys import platform
from pathlib import Path
from unittest.mock import patch

from openlatestscreenshot import windows


@pytest.mark.skipif(platform != "win32", reason="only runs on Windows")
def test_get_screenshot_capture_date_from_filepath_windows_invalid_filepath():
    """Test when filepath ISN'T valid regex on Windows."""
    invalid_windows_filename = "will not pass regex.png"

    invalid_filename_timestamp = windows.get_screenshot_capture_date(
        Path(invalid_windows_filename)
    )

    assert invalid_filename_timestamp == None


@pytest.mark.skipif(platform != "win32", reason="only runs on Windows")
@patch("pathlib.Path.stat")
def test_get_screenshot_capture_date_from_filepath_windows_valid_filepath(
    mock_stat,
):
    """Test when filepath IS good on Windows."""
    mock_stat.return_value.st_mtime = 1708923118.2238703
    valid_windows_filename = "Screenshot blah blah blah.png"
    # raise Exception()
    valid_filename_timestamp = windows.get_screenshot_capture_date(
        Path(valid_windows_filename)
    )

    assert valid_filename_timestamp == 1708923118.2238703


# FIXME: test when filepath ISN'T an image on Windows
