from pathlib import Path


# NOTE: Path() uses __new__ instead of __init__, so the approach is a bit wacky here (see: https://stackoverflow.com/questions/61689391/error-with-simple-subclassing-of-pathlib-path-no-flavour-attribute)
class MockPath(type(Path())):
    def __new__(cls, *pathsegments, is_file_override):
        _path = super().__new__(cls, *pathsegments)
        _path.is_file = is_file_override
        return _path
