Below is not necessary as GitLab automatically passes along artifacts to following pipeline stages (as far as I understand it). Since it was a pain to figure out the curl command however, it's noted here for reference in case it's useful for something like pulling an artifact from a different project or commit.

```yml
    # install unzip for unzipping artifact archive
    - apt-get update && apt-get install -y unzip
    # NOTE: just for debugging
    - echo "${CI_PROJECT_PATH}/-/jobs/artifacts/${CI_COMMIT_BRANCH}"
    # download latest build artifact
    - curl --location --output artifacts.zip "https://gitlab.com/${CI_PROJECT_PATH}/-/jobs/artifacts/${CI_COMMIT_BRANCH}/download?job=build"
    # unzip artifacts.zip
    - unzip artifacts.zip
```
