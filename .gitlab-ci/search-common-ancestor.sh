#!/bin/bash

set -e

# ISSUE: used with `--shallow-since`, however doesn't appear to be properly supported by GitLab as far as I can tell (at least without non-free licenses or self-hosting)
# ancestor_horizon=28  # days (4 weeks)
ancestor_depth=200  # commits

# Recently, git is picky about directory ownership. Tell it not to worry.
git config --global --add safe.directory "$PWD"

# We need to add a new remote for the upstream target branch, since this script
# could be running in a personal fork of the repository which has out of date
# branches.
#
# Limit the fetch to a certain date horizon to limit the amount of data we get.
# If the branch was forked from origin/main before this horizon, it should
# probably be rebased.
if ! git ls-remote --exit-code upstream >/dev/null 2>&1 ; then
    git remote add upstream https://gitlab.com/DrTexx/open-latest-screenshot.git
fi

# Work out the newest common ancestor between the detached HEAD that this CI job
# has checked out, and the upstream target branch (which will typically be
# `upstream/main`).
# `${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}` or `${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}`
# are only defined if we’re running in a merge request pipeline,
# fall back to `${CI_DEFAULT_BRANCH}` or `${CI_COMMIT_BRANCH}` respectively
# otherwise.

source_branch="${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME:-${CI_COMMIT_BRANCH}}"
target_branch="${CI_MERGE_REQUEST_TARGET_BRANCH_NAME:-${CI_DEFAULT_BRANCH}}"

# NOTE: this fetch will fail if your "origin" remote belongs to a private repository (or is otherwise not visible to the Gitlab runner for whatever reason/s)
# ISSUE: using `--shallow-since` doesn't appear to be properly supported by GitLab as far as I can tell (at least without non-free licenses or self-hosting)
#git fetch --shallow-since="$(date --date="${ancestor_horizon} days ago" +%Y-%m-%d)" origin "${source_branch}"
git fetch --depth=${ancestor_depth} origin "${source_branch}"
# NOTE: if your "upstream" remote uses https instead of ssh, you may encounter this error: `fatal: error processing shallow info: 4` - note that as far as I can gather this is just a different error for the same issue caused by GitLab not liking it when we try to do a `git fetch` using the `--shallow-since` arg, from what I can tell it's just a different error for HTTPS remotes vs SSH remotes (see "workaround" (but not really in this case) at https://confluence.atlassian.com/bitbucketserverkb/git-clone-fails-fatal-the-remote-end-hung-up-unexpectedly-fatal-early-eof-fatal-index-pack-failed-779171803.html)
# NOTE: this fetch will fail if your "upstream" remote belongs to a private repository (or is otherwise not visible to the Gitlab runner for whatever reason/s)
# ISSUE: using `--shallow-since` doesn't appear to be properly supported by GitLab as far as I can tell (at least without non-free licenses or self-hosting)
#git fetch --shallow-since="$(date --date="${ancestor_horizon} days ago" +%Y-%m-%d)" upstream "${target_branch}"
git fetch --depth=${ancestor_depth} upstream "${target_branch}"

newest_common_ancestor_sha=$(git merge-base "upstream/${target_branch}" "origin/${source_branch}" || echo "")

if [ -z "${newest_common_ancestor_sha}" ]; then
    echo "Couldn’t find common ancestor with upstream main branch. This typically"
    echo "happens if you branched from main a long time ago. Please update"
    echo "your clone, rebase, and re-push your branch."
    exit 1
fi
