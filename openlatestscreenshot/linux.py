# built in
import re
from datetime import datetime, timezone


SCREENSHOT_FILENAME_DATE_CAPTURE_REGEX_LINUX = (
    r"Screenshot from (\d{4}-\d{2}-\d{2} \d{2}-\d{2}-\d{2})\.png"
)


def get_screenshot_capture_date(filepath):
    # search for a date using the screenshot filename regex
    screenshot_filename = re.fullmatch(
        SCREENSHOT_FILENAME_DATE_CAPTURE_REGEX_LINUX, filepath.name
    )
    # if the filename matches the screenshot filename regex
    if screenshot_filename is not None:
        # get date from filename as a string
        date_str = screenshot_filename.group(1)
        # convert date string to datetime object
        date = datetime.strptime(date_str, "%Y-%m-%d %H-%M-%S")
        # get datetime as utc posix timestamp for simple comparison
        return date.replace(tzinfo=timezone.utc).timestamp()
    else:
        return None
