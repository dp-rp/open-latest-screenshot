from datetime import datetime


def handle_new_latest_screenshot(
    latest_utc_timestamp, latest_screenshot_filepath
):
    print(
        f"{latest_utc_timestamp} ({datetime.utcfromtimestamp(latest_utc_timestamp).strftime('%Y-%m-%d %H-%M-%S')}) is now the most recent screenshot found! ({latest_screenshot_filepath.name})"
    )
