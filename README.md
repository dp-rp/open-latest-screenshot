[![pipeline status](https://gitlab.com/DrTexx/open-latest-screenshot/badges/main/pipeline.svg)](https://gitlab.com/DrTexx/open-latest-screenshot/-/commits/main)
[![coverage report](https://gitlab.com/DrTexx/open-latest-screenshot/badges/main/coverage.svg)](https://gitlab.com/DrTexx/open-latest-screenshot/-/commits/main)
[![GitLab issues](https://img.shields.io/gitlab/issues/open/drtexx/open-latest-screenshot?logo=gitlab)](https://gitlab.com/DrTexx/open-latest-screenshot/-/issues)
[![GitLab merge requests](https://img.shields.io/gitlab/merge-requests/open/drtexx/open-latest-screenshot?logo=gitlab)](https://gitlab.com/DrTexx/open-latest-screenshot/-/merge_requests)

# Open Latest Screenshot

Opens the latest screenshot taken in your default image viewer.

[![PyPI Release](https://img.shields.io/pypi/v/openlatestscreenshot?logo=python)](https://pypi.org/project/openlatestscreenshot)
[![PyPI - Downloads](https://img.shields.io/pypi/dm/openlatestscreenshot?logo=Python)](https://pypi.org/project/openlatestscreenshot)
[![PyPI - License](https://img.shields.io/pypi/l/openlatestscreenshot?color=orange&logo=Python)](https://pypi.org/project/openlatestscreenshot)

## Getting Started

Follow the [install instructions](#installation), then see [usage](#usage).

## Requirements

All popular Linux distributions with `Python 3.8` or later are currently supported.

If you're on Linux and you find `open-latest-screenshot` doesn't work on your distro, please [open a new issue here](https://gitlab.com/DrTexx/open-latest-screenshot/-/issues).

## Installation

```bash
pip install openlatestscreenshot
```

## Usage

```bash
open-latest-screenshot
```

### Keyboard Shortcut Setup

If you find yourself using `open-latest-screenshot` a lot, it's recommended to set a **custom keyboard shortcut** to launch the script. Many popular Linux desktop environments allow you to do this easily.

### Gnome Keyboard Shortcut Setup
1. Open GNOME settings
2. Navigate to `Keyboard > Keyboard Shortcuts > Customize Shortcuts > Custom Shortcuts`
3. Click the `Add Shortcut` button
4. Set the `name`, `command` and `shortcut` to the following:
   - `name`: `Open latest screenshot`
   - `command`: `open-latest-screenshot`
   - `shortcut`: Whatever you like!
5. Done!

**TIP:** If the keyboard shortcut doesn't seem to be working, make sure to test that `open-latest-screenshot` is working as expected from your terminal, as GNOME won't let you know if an error has occured.

## Development

### Installation (for Developers)

```bash
cd "<repo_root_dir>"
poetry install
```

### Usage (for Developers)

```bash
cd "<repo_root_dir>"
poetry run open-latest-screenshot
```

### Generating Coverage Reports Locally

```bash
cd "<repo_root_dir>"
./development-scripts/generate-html-coverage-report.sh  # generate HTML coverage report
start "htmlcov/index.html"  # open coverage report in default browser
```

**Important Note:** Coverage results will only be updated each time the `generate-html-coverage-report.sh` script is run. If you've added/updated tests but your coverage hasn't changed, make sure that you haven't forgotten to rerun the script.

### Build/Test

Builds and tests are automated by GitLab pipelines!

Just [create a merge request](https://gitlab.com/DrTexx/open-latest-screenshot/-/merge_requests/new) and everything should happen automatically ⚙️ ✅

## Links

<!-- TODO: add website link -->
- 📖 &nbsp;[Documentation](https://gitlab.com/DrTexx/open-latest-screenshot)
- 🐍 &nbsp;[Latest Release](https://pypi.org/project/openlatestscreenshot)
- 🧰 &nbsp;[Source Code](https://gitlab.com/DrTexx/open-latest-screenshot)
- 🐞 &nbsp;[Issue Tracker](https://gitlab.com/DrTexx/open-latest-screenshot/-/issues)
- `🐦 Twitter` &nbsp;[@DrTexx](https://twitter.com/DrTexx)
- `📨 Email` &nbsp;[denver.opensource@tutanota.com](mailto:denver.opensource@tutanota.com)
