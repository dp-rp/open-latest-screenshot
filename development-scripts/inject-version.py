import toml

PYPROJECT_TOML_FILEPATH = "pyproject.toml"
PACKAGE_NAME = "openlatestscreenshot"
DEBUG = False

package_version_filepath = PACKAGE_NAME + "/__version__.py"

with open(PYPROJECT_TOML_FILEPATH, "r") as f:
    if DEBUG:
        print(f"INJECT VERSION: Opening '{PYPROJECT_TOML_FILEPATH}'...")
    pyproject = toml.loads(f.read())
    if DEBUG:
        print(
            f"INJECT VERSION: Extracting version number from '{PYPROJECT_TOML_FILEPATH}'..."
        )
    version = pyproject["tool"]["poetry"]["version"]
    if DEBUG:
        print(f"INJECT VERSION: Opening '{package_version_filepath}'...")
    with open(package_version_filepath, "w") as version_file:
        if DEBUG:
            print(
                f"INJECT VERSION: Writing version number to '{package_version_filepath}'..."
            )
        version_file.write(
            "# NOTE: This script is auto-generated each build based on the version in pyproject.toml!"
            + "\n"
            + f'\n__version__ = "{version}"'
            + "\n"
        )

print(
    f"INJECT VERSION: Version number from '{PYPROJECT_TOML_FILEPATH}' ({version}) successfully written to '{package_version_filepath}'!"
)
